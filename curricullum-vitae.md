# CURRICULLUM VITAE
Copyright 2017 Resume Muhamad Syafii. All right reserved.


# Profile
- Full Names : Muhamad Syafii
- Birthday : Cirebon, 08 Agustus 1996

# Contact's
- Telegram        : @fii.upl 
- Phone           : +6282217006113
- Email           : - muhamadsyafii4@gmail.com
                    - muhamadsyafii100@gmail.com
                    - muhamadsyafii@mhs.pelitabangsa.ac.id

# Interesting & Hobby's

- Write Code
- Oprek Software
- Gaming
- Linux
- Blogging
- Writing
- Reading
- Education's

# Education's
- STT Pelita Bangsa Cikarang | Teknik Informatika | 2015-Now
- SMK Bina Taruna Jalancagak Subang | Teknik Informatika | 2011-2014
- SMP Negeri 1 Sagalaherang | 2009-2011
- SDN Tengger Agung | 2003-2009

# Skill's

- Ms.Office | *****
- Repair Hardware & Software | *****
- Windows | ******
- Linux | *****
- Networking | ***

# Portofolio's
- Websites: https://www.muhamadsyafii.ml/

# 
